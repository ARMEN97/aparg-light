noUiSlider.create(slider_obj, {
	step : 1,
	start: 0,
	connect: 'lower',
	range: {
		'min': 0,
		'max': MAX
	}
});

slider_obj.noUiSlider.on('slide', function (data) {
	var value	= data[0];
	var percent	= Math.floor((value * 100) / MAX);
});