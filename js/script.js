var Timer   = [];
var Socket  = new WebSocket('ws://192.168.0.115:1122');

$(document).ready(function () {





    Socket.onopen = function () {
        sendCommand('getAnimation', {});
        clearInterval(Timer);
        $(".overlay").addClass("none");
        $("#connect").removeClass('connectClose').addClass("connectOpen")
    }
    
    Socket.onmessage = function (data) {
        var data = JSON.parse(data.data);

        if ('Animation' in data) {
            $('input.slide.brightness').val(data.Animation.Brightness);
            $('input.slide.speed').val(data.Animation.Speed);
            $('select.animation').val(data.Animation.Mode).formSelect();
            $('input.radio.' + (data.Animation.Direction == 0 ? 'down' : 'up') ).attr('checked', 'checked');
            $('input.onOf').prop('checked', data.Animation.Mode != 0);
        }
    }

    Socket.onclose = function(){
        $("#connect").removeClass('connectOpen').addClass("connectClose");
        Timer = setInterval(function () {
            Socket = new WebSocket('ws://192.168.0.115:1122');
        }, 10000)
    }

    $(document)
        .on('change', 'input.onOf', function () {
            var checked = $(this).is(':checked');
            var Mode    = checked ? 1 : 0;
            var Data    = {
                'Mode': Mode
            };

            if(!checked) {
                Data['Speed'] = 0;   
            }
            sendCommand('setAnimation', Data);
            $('select.animation').val(Mode).formSelect();
        })
        .on('change', 'input.slide.brightness', function () {
            sendCommand('setAnimation', {
                'Brightness': this.value
            });
        })
        .on('change', 'input.slide.speed', function () {
            sendCommand('setAnimation', {
                'Speed': $(".speed").attr("max") - this.value
            })
        })
        .on('change', 'input.radio', function () {
            sendCommand('setAnimation', {
                'Direction': this.value
            });
        })
        .on('change', 'select.animation', function() {
            if( $('input.onOf').is(':checked') ) {
                sendCommand('setAnimation', {
                    'Mode': this.value
                });
            }
        })
    ;
});


function sendCommand(action, params) {
    var Obj = {
        'action': action,
        'params': params
    };

    Socket.send(JSON.stringify(Obj));
}